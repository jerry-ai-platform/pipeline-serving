import os
import time
import requests
import argparse
import http.client, urllib.parse
import json

handler_host="http://api.ai-platform.org/pipeline-handler/"
backend_host="http://api.ai-platform.org/backend/serving/create"
tensorboard_host="http://api.ai-platform.org/backend/tensorboard/create"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Kubeflow FMNIST training script')
    parser.add_argument('--bucket_name', help='buckut_name')
    parser.add_argument('--user', help='user_name')
    parser.add_argument('--serv_name', help='serv_name')
    args = parser.parse_args()
    
    bk = str(args.bucket_name)
    user = str(args.user)
    servn = str(args.serv_name)

    print("Serving instruction start...")
    print("====== info =======")
    print("user: ", user)
    print("bucket: ", bk)
    print("model_name:", servn)

    r = requests.get(handler_host+'serving?bk='+bk+"&user="+user+"&servn="+servn)

    print("Loading",end = "")
    for i in range(4):
        print(".",end = '',flush = True)
        time.sleep(0.5)

    print('\n')
    data = r.json()
    print(data)

    print("Launch Serving pod")
    r = requests.post(backend_host, json={"username": str(user), "modelname": str(servn)})
    print('\n')
    data = r.json()
    print(data)

    print("Launch Tensorboard")
    r = requests.post(tensorboard_host, json={
                                                "username": str(user), 
                                                "runname": str(servn),
                                                "bucketname" : str(bk),
                                                "logdir" : "save_model"
                                            })
    print('\n')
    data = r.json()
    print(data)
