# example on how to build docker:
# DOCKER_BUILDKIT=1 docker build -t benjamintanweihao/kubeflow-mnist . -f Dockerfile
# DOCKER_BUILDKIT=1 docker build --no-cache -t benjamintanweihao/kubeflow-mnist env -f Dockerfile

# example on how to run:
# docker run -it benjamintanweihao/kubeflow-mnist /bin/bash

FROM python:3.8.11-slim-buster
LABEL MAINTAINER "Kerwin Tsai"
SHELL ["/bin/bash", "-c"]


RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    git &&\
    rm -rf /var/lib/apt/lists/*

# install dep.
RUN pip3 install requests
RUN git clone https://gitlab.com/jerry-ai-platform/pipeline-serving /serving && cd /serving && git pull